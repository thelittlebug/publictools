#!/bin/bash

version=$(lsb_release -cs)
#version=xenial

apt-get --yes --force-yes remove docker docker-engine
apt-get --yes --force-yes install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $version stable"
apt-get update
apt-get --yes --force-yes upgrade
apt-get --yes --force-yes install docker-ce

#apt-get update && apt-get install curl
#curl -sSL https://get.docker.com/ | sh
#curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
#chmod +x /usr/local/bin/docker-compose